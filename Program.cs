﻿using System;
using System.IO;
using System.Text.Json;

namespace ДЗ10
{
    public class Inf
    {
        public string N { get; set; }
        public string F { get; set; }
        public int V { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите имя:");
            string N1 = Console.ReadLine();
            Console.WriteLine("Введите фамилию:");
            string F1 = Console.ReadLine();
            Console.WriteLine("Введите возвраст:");
            int V1 = Convert.ToInt32(Console.ReadLine());

            Inf inf1 = new Inf()
            {
                N = N1,
                F = F1,
                V = V1
            };

            string Inf1 = JsonSerializer.Serialize(inf1, typeof(Inf));
            StreamWriter file = File.CreateText("Inf.json");
            file.WriteLine(Inf1);
            file.Close();
            Console.WriteLine("Данные записанные в файл после сериализации:");
            Console.WriteLine(Inf1);

            Console.WriteLine(new string('-', 50));

            if (File.Exists("Inf.json"))
            {
                string i = File.ReadAllText("Inf.json");
                Inf Inf2 = JsonSerializer.Deserialize<Inf>(i);
                Console.WriteLine("Данные десериализированные из файла:");
                Console.WriteLine($"Имя: {inf1.N}");
                Console.WriteLine($"Фамилия: {inf1.F}");
                Console.WriteLine($"Возраст: {inf1.V}");
            }
            else
            {
                Console.WriteLine("Ошибка десериализации");
            }
        }
    }
}
